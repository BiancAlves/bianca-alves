import React from 'react';

//Componentes
import Image from '../components/Image.jsx';

//Imagens
import LogoLightImg from '../img/logo-light.png';

// Estilo
import '../styles/footer.scss';

export default props => (
	<footer className="footer">
		<div className="footer-left">
			<Image source={LogoLightImg} customClass="logo" />
		</div>
		<div className="footer-right">
			<p>Agência N1 - Todos os direitos reservados</p>
		</div>

	</footer>
)