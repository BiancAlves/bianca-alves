import React from 'react';

// Componentes
import Image from '../components/Image.jsx';
import Button from '../components/Button.jsx';

// Imagens
import CheckImg from '../img/check.png';

// Estilo
import '../styles/modal.scss';

export default props => (
	<div id="modal" className="modal-bg">
		<div className="modal">
			<Image source={CheckImg} />

			<p>Produto adicionado ao carrinho</p>
			<Button text="Ok!" style="dark" uppercase="true" handleClick={props.toggleModal}/>
		</div>
	</div>
)