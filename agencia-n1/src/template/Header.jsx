import React from 'react';

// Componentes
import Image from '../components/Image.jsx';
import SearchForm from '../components/SearchForm.jsx';
import Menu from '../components/Menu.jsx';

//Imagens
import LogoImg from '../img/logo.png';
import CartImg from '../img/cart.png';

// Estilo
import '../styles/header.scss';

export default props => (
	<header>
		<div className="container">
			<div className="header-left">
				<Image source={LogoImg} customClass="logo" hrefer="#" alt="Agência N1"/>
				<Menu />
			</div>

			<div className="header-right">
				<SearchForm />

				<div className="cart">
					<Image source={CartImg} hrefer="#" alt="Carrinho"/>
					<div id="cart-counter" className="counter">0</div>
				</div>
			</div>
		</div>
	</header>
)