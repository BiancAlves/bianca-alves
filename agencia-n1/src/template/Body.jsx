import React, {Component} from 'react';

//Componentes
import Image from '../components/Image.jsx';
import Breadcrumb from '../components/Breadcrumb.jsx';
import Button from '../components/Button.jsx';
import ProductImage from '../components/ProductImage.jsx';
import ProductBox from '../components/ProductBox.jsx';

//Imagens
import MarioBombeiroImg from '../img/mario-bombeiro.png';
import DoctorStrangeImg from '../img/doctor-strange.png';
import MarioImg from '../img/mario.png';
import RyuImg from '../img/ryu.png';

// Estilo
import '../styles/body.scss';

export default props => (
	<div className="container">
		<Breadcrumb links="N1, action figures, Super Mario"/>

		<section className="section product-main">
			<ProductImage source={MarioBombeiroImg} alt="Mário bombeiro" />

			<div className="product-details">
				<h1>Action Figure bombeiro Mario topzeira das galáxias</h1>
				<div className="price">
					<p>de R$ 189,90</p>
					<p>por <span className="big">R$ 149,90</span></p>
					<Button text="Compra Ae" uppercase="true" big="true" style="success" handleClick={props.toggleModal} />
				</div>

				<div className="frete">
					<p>Calcule o frete</p>
					<form action="#">
						<input type="text" placeholder="00000" />
						<input type="text" placeholder="000" className="small" />
						<Button buttonType="submit" style="dark" text="Calcular" />
					</form>
				</div>
			</div>
		</section>

		<section className="section product-description">
			<h2>Descrição do produto</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus lacinia enim eget risus porttitor auctor. Quisque quis magna sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc pulvinar, lacus sit amet dignissim ultrices, turpis libero euismod sapien, sed pretium ligula libero eu felis. Sed sapien sem, maximus sit amet consequat luctus, dictum vel quam. Fusce malesuada suscipit faucibus. Suspendisse sit amet dui a nisi pretium lacinia efficitur nec sem. Aliquam rhoncus eget lacus a vestibulum. Donec congue lorem commodo ante aliquet, et interdum leo volutpat. Proin ornare diam porttitor, sollicitudin est efficitur, pretium eros. Vestibulum sit amet consectetur eros, vel euismod leo. Cras eget consectetur turpis, ac aliquam augue.</p>
		 	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus lacinia enim eget risus porttitor auctor. Quisque quis magna sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
		</section>

		<section className="section related-products">
			<h2>Quem viu, viu também</h2>
			<div className="products">
				<ProductBox imgSource={DoctorStrangeImg} description="Action Figure Doctor Strange e Burning Flame Set - S.H.Figuarts" oldPrice="725,90" newPrice="624,90" />
				<ProductBox imgSource={MarioImg} description="Action Figures - Super Mario Bros - Bandal" oldPrice="189,90" newPrice="149,90"  />
				<ProductBox imgSource={RyuImg} description="Figura Street Fighter Ryu"  oldPrice="5.500,20" newPrice="5.259,00" />
			</div>
		</section>
	</div>
)