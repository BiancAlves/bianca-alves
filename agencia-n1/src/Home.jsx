import React, {Component} from 'react';
import Header from './template/Header.jsx';
import Body from './template/Body.jsx';
import Footer from './template/Footer.jsx';
import Modal from './template/Modal.jsx';

export default class Home extends Component {
	// Fazendo a modal funcionar

	constructor(){
		super();

		this.toggleModal = this.toggleModal.bind(this);
		this.state = {
			modal: true
		}

		this.cartCounter = 0;
	}

	addToCart(){
		let counter = document.getElementById('cart-counter');
		this.cartCounter++;
		counter.innerHTML = this.cartCounter;
	}

	toggleModal(){
		this.state.modal = !this.state.modal;
		var htmlTag = document.getElementsByTagName('html')[0];

		if(this.state.modal){
			document.getElementById('modal').className = "modal-bg";
			htmlTag.className = ''
		} else {
			document.getElementById('modal').className = "modal-bg active"
			htmlTag.className = 'modal-active';
			window.scrollTo(0, 0);
			this.addToCart();
		}
	}

	render(){
		return (
			<div className="home">
				<Header />
				<Body toggleModal={this.toggleModal}/>
				<Footer />
				<Modal toggleModal={this.toggleModal}/>
			</div>
		)
	}
}