import React from 'react';

export default props => {
	let links = props.links.split(',');
	let jsx = [];
	
	
	links.forEach((item, index)=>{
		if(index + 1 === links.length){
			jsx.push(<div className="item"><span>{item}</span></div>); 	
		} else {
			jsx.push(<div className="item"><a href="#">{item}</a></div>); 
		}
		
	});

	return (
		<div className="breadcrumb">
			{jsx}
		</div>
	);
}