import React, {Component} from 'react';

export default class ProductImage extends Component {
	// Zoom no produto onmoushover
	zoomIn(event) {
		if(document.body.clientWidth > 992){
			var element = document.getElementById("product-image");
			element.style.display = "inline-block";
			var img = document.getElementById("product-mini");
			var posX = event.pageX - (img.offsetLeft * 3);
			var posY = event.pageY - (img.offsetTop * 3);
			element.style.backgroundPosition=(-posX*2)+"px "+(-posY*4)+"px";
			element.style.transform = "scale(1.5)";
		}
	}

	zoomOut() {
		var img = document.getElementById("product-image");
		img.style.backgroundPosition = "center";
		img.style.transform = "scale(1)";
	}

	render(){
		const productImage = this.props.source;
		
		const productStyle = {
			backgroundImage: 'url(' + productImage + ')'
		};

		return (
			<div className="product-images">
				<figure id="product-mini" className="img-wrap product-mini">
					<img src={productImage} alt={this.props.alt}/>
				</figure>
				<div className="product-image-wrapper">
					<div id="product-image" className="product-big" onMouseMove={this.zoomIn} onMouseOut={this.zoomOut} style={productStyle}></div>
				</div>
			</div>
		)
	}
}