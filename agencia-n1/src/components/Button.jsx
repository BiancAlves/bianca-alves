import React from 'react';

export default props => {
	let classes = '';
	
	if(props.uppercase) {
		classes += ' uppercase';
	}

	if (props.big){
		classes += ' btn-big';
	}

	return <button type={props.buttonType} className={'btn btn-' + props.style + classes} onClick={props.handleClick}>{props.text}</button>
};