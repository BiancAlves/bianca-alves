import React from 'react';
import Image from './Image.jsx'

export default props => (
	<div className="product-box">
		<a href="#">
			<Image source={props.imgSource} />
			<p className="description">{props.description}</p>
			<div className="price">
				<p>de R${props.oldPrice}</p>
				<p className="new-price">por R${props.newPrice}</p>
			</div>
		</a>
	</div>
);