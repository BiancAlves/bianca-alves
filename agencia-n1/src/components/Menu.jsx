import React, {Component} from 'react';

export default class Menu extends Component {
	constructor(){
		super();

		this.state = {
			condition: false
		}
	}

  handleClick =  (e) => {
  	if(e.target.className.indexOf("menu-button") > -1 || e.target.className.indexOf("menu-wrapper") > -1 || e.target.parentNode.className.indexOf('menu-button') > -1){
  		this.setState({
	      condition: !this.state.condition
	    });

	    var htmlTag = document.getElementsByTagName('html')[0];
	    if(!this.state.condition){
	    	htmlTag.className = 'menu-active';
	    } else {
	    	htmlTag.className = '';
	    }
  	}
  }

	render (){
		return (
			<div className="menu">
				<div onClick={ this.handleClick } className={this.state.condition ? "menu-button active" : "menu-button"}>
					<span> </span>
					<span> </span>
					<span> </span>
				</div>
				<div onClick={ this.handleClick } className={this.state.condition ? "menu-wrapper active" : "menu-wrapper"}>
					<ul className="menu-list">
						<li><a href="#">Games</a></li>
						<li><a href="#">Presentes</a></li>
						<li className="highlight"><a href="#">Sale</a></li>
					</ul>
				</div>
			</div>
		)
	}
}